import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;


class q4654 {

	static final String TESTFILENAME = "lorem_ipsum.txt";

	public static void main(String[] args) {
		StringBuffer sb = new StringBuffer();
		try {
			File file = new File(TESTFILENAME);
			BufferedReader br = new BufferedReader(new FileReader(file));

			String str;
			while((str = br.readLine()) != null) {
				sb.append(str);
			}

			br.close();
		} catch (Exception e) {
			System.out.println("catch exception " + e.getMessage());
		}

		System.out.println("tail " + sb.toString());
	}

}
